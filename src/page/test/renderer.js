const { ipcRenderer } = require('electron');

document.querySelector('#home')
    .addEventListener('click', () => {
        ipcRenderer.send('page-home');
    });

//https://the-trivia-api.com/
const question = fetch('https://the-trivia-api.com/v2/questions',{
    headers: {
        'Accept': 'application/json',
        'Content': 'application/json'
    }

}).then((response) => {
    return response.json();

}).then((data) => {
    data.forEach((e) =>{
        console.log(e.category);
    });
});

